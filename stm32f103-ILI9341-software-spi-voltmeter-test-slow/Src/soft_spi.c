#include "stdint.h"
#include "bband.h"
#include "pins_ext.h"
#include "soft_spi.h"

GPIO_TypeDef *mosi_port;
GPIO_TypeDef *miso_port;
GPIO_TypeDef *clock_port;
GPIO_TypeDef *cs_port;

uint16_t mosi_pin;
uint16_t miso_pin;
uint16_t clock_pin;
uint16_t cs_pin;

void delay_tics(uint32_t tics)
{
  uint32_t count=0;
  for(count=0;count<tics;count++)
  {
    __asm volatile ("nop");
  }
}

void soft_spi_pins(uint16_t _mosi, uint16_t _miso, uint16_t _clock, uint16_t _cs)
{
 mosi_pin=_mosi;
 miso_pin=_miso;
 clock_pin=_clock;
 cs_pin=_cs;
}

void soft_spi_ports(GPIO_TypeDef *_mosi_prt,GPIO_TypeDef *_miso_prt,GPIO_TypeDef *_clock_prt,GPIO_TypeDef *_cs_prt)
{
  mosi_port=_mosi_prt;
  miso_port=_miso_prt;
  clock_port=_clock_prt;
  cs_port=_cs_prt;
}

void soft_spi_init(void)
{
  _delay = 2;
  clock_phase = 0;
  clock_polar = 0;
}

void soft_spi_begin(void)
{
  pin_mode(mosi_port, mosi_pin, GPIO_MODE_OUTPUT_PP);
  pin_mode(miso_port, miso_pin, GPIO_MODE_INPUT);
  pin_mode(clock_port, clock_pin, GPIO_MODE_OUTPUT_PP);
  pin_mode(cs_port, cs_pin, GPIO_MODE_OUTPUT_PP);
}

void soft_spi_end(void)
{
  HAL_GPIO_DeInit(mosi_port, mosi_pin);
  HAL_GPIO_DeInit(miso_port, miso_pin);
  HAL_GPIO_DeInit(clock_port, clock_pin);
  //HAL_GPIO_DeInit(cs_port, cs_pin);
}

void soft_spi_set_bit_order(uint8_t order)
{
  switch (order)
  {
    case SPI_LSB_FIRST:
    _order = SPI_LSB_FIRST;
    break;
    case SPI_MSB_FIRST:
    _order = SPI_MSB_FIRST;
    break;
  }
}

void soft_spi_set_data_mode(uint8_t mode)
{
  switch (mode)
  {
    case SPI_MODE0:
      clock_polar = 0;
      clock_phase = 0;
    break;
    case SPI_MODE1:
      clock_polar = 0;
      clock_phase = 1;
    break;
    case SPI_MODE2:
      clock_polar = 1;
      clock_phase = 0;
    break;
    case SPI_MODE3:
      clock_polar = 1;
      clock_phase = 1;
    break;
  }
}

void soft_spi_set_clock_divider(uint32_t div)
{
  _delay = div;
}

uint8_t soft_spi_transfer(uint8_t val)
{
  uint8_t out = 0;
  if(_order == SPI_MSB_FIRST)
  {
    uint8_t v2 =
    ((val & 0x01) << 7) |
    ((val & 0x02) << 5) |
    ((val & 0x04) << 3) |
    ((val & 0x08) << 1) |
    ((val & 0x10) >> 1) |
    ((val & 0x20) >> 3) |
    ((val & 0x40) >> 5) |
    ((val & 0x80) >> 7);
    val = v2;
  }
  uint32_t del = _delay >> 1;
  uint8_t bval = 0;
  for (uint8_t bit = 0; bit < 8; bit++)
  {
    if(clock_polar) pin_write(clock_port, clock_pin, 1); else pin_write(clock_port, clock_pin, 0);
    delay_tics(del);
    if(clock_phase)
    {
      bval = pin_read(miso_port, miso_pin);
      if(_order == SPI_MSB_FIRST)
      {
        out <<= 1;
        out |= bval;
      }
      else
      {
        out >>= 1;
        out |= bval << 7;
      }
    }
    else
    {
      if(val & (1<<bit)) (pin_write(mosi_port, mosi_pin, 1)); else (pin_write(mosi_port, mosi_pin, 0));
    }
    delay_tics(del);
    if(clock_polar) pin_write(clock_port, clock_pin, 0); else pin_write(clock_port, clock_pin, 1);
    delay_tics(del);
    if(clock_phase)
    {
      if(val & (1<<bit)) (pin_write(mosi_port, mosi_pin, 1)); else (pin_write(mosi_port, mosi_pin, 0));
    }
    else
    {
      bval = pin_read(miso_port, miso_pin);
      if(_order == SPI_MSB_FIRST)
      {
        out <<= 1;
        out |= bval;
      }
      else
      {
        out >>= 1;
        out |= bval << 7;
      }
    }
    delay_tics(del);
  }
  return out;
}

uint16_t soft_spi_transfer16(uint16_t val)
{
  uint16_t out = 0;
  if(_order == SPI_MSB_FIRST)
  {
    uint16_t v2 =
    ((val & 0x01) << 15) |
    ((val & 0x02) << 13) |
    ((val & 0x04) << 11) |
    ((val & 0x08) << 9) |
    ((val & 0x10) << 7) |
    ((val & 0x20) << 5) |
    ((val & 0x40) << 3) |
    ((val & 0x80) << 1) |
    ((val & 0x100) >> 1) |
    ((val & 0x200) >> 3) |
    ((val & 0x400) >> 5) |
    ((val & 0x800) >> 7) |
    ((val & 0x1000) >> 9) |
    ((val & 0x2000) >> 11)|
    ((val & 0x4000) >> 13)|
    ((val & 0x8000) >> 15);
    val = v2;
  }
  uint32_t del = _delay >> 1;
  uint16_t bval = 0;
  for (uint16_t bit = 0; bit < 16; bit++)
  {
    if(clock_polar) pin_write(clock_port, clock_pin, 1); else pin_write(clock_port, clock_pin, 0);
    delay_tics(del);
    if(clock_phase)
    {
      bval = pin_read(miso_port, miso_pin);
      if(_order == SPI_MSB_FIRST)
      {
        out <<= 1;
        out |= bval;
      }
      else
      {
        out >>= 1;
        out |= bval << 15;
      }
    }
    else
    {
      if(val & (1<<bit)) (pin_write(mosi_port, mosi_pin, 1)); else (pin_write(mosi_port, mosi_pin, 0));
    }
    delay_tics(del);
    if(clock_polar) pin_write(clock_port, clock_pin, 0); else pin_write(clock_port, clock_pin, 1);
    delay_tics(del);
    if(clock_phase)
    {
      if(val & (1<<bit)) (pin_write(mosi_port, mosi_pin, 1)); else (pin_write(mosi_port, mosi_pin, 0));
    }
    else
    {
      bval = pin_read(miso_port, miso_pin);
      if(_order == SPI_MSB_FIRST)
      {
        out <<= 1;
        out |= bval;
      }
      else
      {
        out >>= 1;
        out |= bval << 15;
      }
    }
    delay_tics(del);
  }
  return out;
}

void soft_spi_write(uint16_t val)
{
  if(_order == SPI_MSB_FIRST)
  {
    uint16_t v2 =
    ((val & 0x01) << 7) |
    ((val & 0x02) << 5) |
    ((val & 0x04) << 3) |
    ((val & 0x08) << 1) |
    ((val & 0x10) >> 1) |
    ((val & 0x20) >> 3) |
    ((val & 0x40) >> 5) |
    ((val & 0x80) >> 7);
    val = v2;
  }
  uint32_t del = _delay >> 1;
  for (uint16_t bit = 0; bit < 8; bit++)
  {
    if(clock_polar) pin_write(clock_port, clock_pin, 1); else pin_write(clock_port, clock_pin, 0);
    delay_tics(del);
    if(val & (1<<bit)) pin_write(mosi_port, mosi_pin, 1); else pin_write(mosi_port, mosi_pin, 0);
    delay_tics(del);
    if(clock_polar) pin_write(clock_port, clock_pin, 0); else pin_write(clock_port, clock_pin, 1);
    delay_tics(del);
    if(val & (1<<bit)) pin_write(mosi_port, mosi_pin, 1); else pin_write(mosi_port, mosi_pin, 0);
    delay_tics(del);
  }
}

void soft_spi_write16(uint16_t val)
{
  if(_order == SPI_MSB_FIRST)
  {
    uint16_t v2 =
    ((val & 0x01) << 15) |
    ((val & 0x02) << 13) |
    ((val & 0x04) << 11) |
    ((val & 0x08) << 9) |
    ((val & 0x10) << 7) |
    ((val & 0x20) << 5) |
    ((val & 0x40) << 3) |
    ((val & 0x80) << 1) |
    ((val & 0x100) >> 1) |
    ((val & 0x200) >> 3) |
    ((val & 0x400) >> 5) |
    ((val & 0x800) >> 7) |
    ((val & 0x1000) >> 9) |
    ((val & 0x2000) >> 11)|
    ((val & 0x4000) >> 13)|
    ((val & 0x8000) >> 15);
    val = v2;
  }
  uint32_t del = _delay >> 1;
  for (uint16_t bit = 0; bit < 16; bit++)
  {
    if(clock_polar) pin_write(clock_port, clock_pin, 1); else pin_write(clock_port, clock_pin, 0);
    delay_tics(del);
    if(val & (1<<bit)) pin_write(mosi_port, mosi_pin, 1); else pin_write(mosi_port, mosi_pin, 0);
    delay_tics(del);
    if(clock_polar) pin_write(clock_port, clock_pin, 0); else pin_write(clock_port, clock_pin, 1);
    delay_tics(del);
    if(val & (1<<bit)) pin_write(mosi_port, mosi_pin, 1); else pin_write(mosi_port, mosi_pin, 0);
    delay_tics(del);
  }
}

void soft_spi_wr(uint16_t val)
{
  uint8_t i;
  for(i = 0; i < 8; i++)
  {
    if(val & 0x80) (pin_write(mosi_port, mosi_pin, 1)); else (pin_write(mosi_port, mosi_pin, 0));
    val = val << 1;
    pin_write(clock_port, clock_pin, 0);
    pin_write(clock_port, clock_pin, 1);
  }
}

void soft_spi_wr16(uint16_t val)
{
  uint8_t i;
  for(i = 0; i < 16; i++)
  {
    if(val & 0x8000) (pin_write(mosi_port, mosi_pin, 1)); else (pin_write(mosi_port, mosi_pin, 0));
    val = val << 1;
    pin_write(clock_port, clock_pin, 0);
    pin_write(clock_port, clock_pin, 1);
  }
}
